<?php
class Library
{
    public function __construct()
    {
        $host = "localhost";
        $dbname = "dbbantuan";
        $username = "root";
        $password = "";
        $this->db = new PDO("mysql:host={$host};dbname={$dbname}", $username, $password);
    }
    public function add_data($no_kk, $nama, $alamat, $telepon, $kondisi)
    {
        $data = $this->db->prepare('INSERT INTO warga (no_kk,nama,alamat,telepon,kondisi) VALUES (?, ?, ?, ?, ?)');
        
        $data->bindParam(1, $no_kk);
        $data->bindParam(2, $nama);
        $data->bindParam(3, $alamat);
        $data->bindParam(4, $telepon);
        $data->bindParam(5, $kondisi);

        $data->execute();
        return $data->rowCount();
    }
    public function show()
    {
        $query = $this->db->prepare("SELECT * FROM warga");
        $query->execute();
        $data = $query->fetchAll();
        return $data;
    }

    public function get_by_id($no_kk){
        $query = $this->db->prepare("SELECT * FROM warga where no_kk=?");
        $query->bindParam(1, $no_kk);
        $query->execute();
        return $query->fetch();
    }

    public function update($no_kk, $nama, $alamat, $telepon, $kondisi){
        $query = $this->db->prepare('UPDATE warga set nama=?,alamat=?,telepon=?,kondisi=? where no_kk=?');
        
        $query->bindParam(1, $nama);
        $query->bindParam(2, $alamat);
        $query->bindParam(3, $telepon);
        $query->bindParam(4, $kondisi);
        $query->bindParam(5, $no_kk);

        $query->execute();
        return $query->rowCount();
    }

    public function delete($no_kk)
    {
        $query = $this->db->prepare("DELETE FROM warga where no_kk=?");

        $query->bindParam(1, $no_kk);

        $query->execute();
        return $query->rowCount();
    }

}
?>