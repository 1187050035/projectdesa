<?php
   session_start();
   if(isset($_SESSION['username'])) {
   header('location:data.php'); }
   require_once("koneksi.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
      <link rel="icon" type="image/png" href="../assets/img/favicon.png">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="style.css">
      <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../assets/css/material-kit.css?v=2.0.7" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/demo/demo.css" rel="stylesheet" />
  <style type="text/css">
		body
		{
			background-image: url(assets/img/521111.jpg);
                        background-position: center;
                        background-repeat: no-repeat;
                        background-size: cover;	
		}
	</style>
      
      <title>Form Login</title>
  </head>
  <body>
  <nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" href="#home">
          Desa Wangunjaya </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a href="utama.php" class="nav-link" >
              <i class="material-icons">apps</i> Data Warga
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://ppawtask.site/login.php">
              <i class="material-icons">unarchive</i> Login
            </a>
        </ul>
      </div>
    </div>
  </nav>
    <div class="wrapper fadeInDown">
      <form action="proseslogin.php" method="post">
        <div id="formContent">
          <!-- Tabs Titles -->
          <h2 class="active"> Sign In </h2>
          <!-- Login Form -->
          <form>
            <input type="text" id="username" class="fadeIn second" name="username" placeholder="Username">
            <input type="password" id="password" class="fadeIn third" name="password" placeholder="Password">
            <input type="submit" class="fadeIn fourth" value="Login">
          </form>
        </div>
      </form>
    </div>
  </body>
</html>