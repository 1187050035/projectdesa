<?php 
include('library.php');
$lib = new Library();
if(isset($_POST['tombol_tambah'])){
    $no_kk = $_POST['no_kk'];
    $nama = $_POST['nama'];
    $alamat = $_POST['alamat'];
    $telepon = $_POST['telepon'];
    $kondisi = $_POST['kondisi'];

    $add_status = $lib->add_data($no_kk, $nama, $alamat, $telepon, $kondisi);
    if($add_status){
    header('Location: data.php');
    }
}
?>
<html>
    <head>
        <title>Tambah Data</title>
         <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    </head>
    <body>
    <div class="container">
        <div class="card mt-4">
            <div class="card-header text-center">
                <h3>Tambah Data Warga<h3>
            </div>
            <div class="card-body">
            <form method="post" action="">
                <div class="form-group row">
                    <label for="no_kk" class="col-sm-2 col-form-label">No KK</label>
                    <div class="col-sm-10">
                    <input type="text" name="no_kk" class="form-control" id="no_kk" placeholder="No KK">
                    </div>
                </div>
                <br>
                <div class="form-group row">
                    <label for="nama" class="col-sm-2 col-form-label">Nama Ayah</label>
                    <div class="col-sm-10">
                    <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Ayah">
                    </div>
                </div> 
                <br>              
                <div class="form-group row">
                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                    <textarea class="form-control" name="alamat" id="alamat" placeholder="Alamat"></textarea>
                    </div>
                </div>
                <br>
                <div class="form-group row">
                    <label for="telepon" class="col-sm-2 col-form-label">No Telepon</label>
                    <div class="col-sm-10">
                    <input type="text" name="telepon" class="form-control" id="telepon" placeholder="No Telepon">
                    </div>
                </div>
                <br>
                <div class="form-group row">
                    <label for="kondisi" class="col-sm-2 col-form-label">Kondisi</label>
                    <div class="col-sm-10">
                    <input type="text" name="kondisi" class="form-control" id="kondisi" placeholder="Sudah Menerima/Belum Menerima">
                    </div>
                </div>
                <br>
                <div class="form-group row">
                    <label for="alamat" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-10">
                    <input type="submit" name="tombol_tambah" class="btn btn-primary" value="Tambah">
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
    </body>
</html>